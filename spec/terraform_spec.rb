require 'spec_helper'

describe Terraform do
  it 'has a version number' do
    expect(Version::VERSION).not_to be nil
  end

  it 'sets config and state' do
    t = Terraform.new 'config', 'state'
    expect(t.config).to eq('config')
    expect(t.state).to eq('state')
  end

  it 'sets'
end
