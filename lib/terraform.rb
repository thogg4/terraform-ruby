require 'cocaine'
require 'logger'
require 'fileutils'

class Terraform
  attr_accessor :config, :state, :output

  def initialize(config=nil, state='')
    raise ArgumentError.new('You must pass in a config string') if !config

    @config = config
    @state = state
  end

  def apply
    run_command('apply')
  end

  def plan
    run_command('plan')
  end

  def destroy
    return 'You must pass in a state to destroy' if @state.empty?
    run_command(['destroy', '-force'].join(' '))
  end

  def run_command(command)
    raise ArgumentError.new('Can\'t find tmp/ directory to write files to') if !Dir.exist?('tmp')

    dir = "tmp/terraform-#{time_now}"
    file = "#{time_now}.tf"

    # create directory
    Dir.mkdir(dir)

    # write config file
    File.open("#{dir}/#{file}", 'w') { |f| f.write(@config) }

    # write state file if we have state
    state_path = "#{dir}/terraform.tfstate"
    if !@state.empty?
      File.open(state_path, 'w') { |f| f.write(@state) }
    end

    line = Cocaine::CommandLine.new("cd #{dir} && terraform #{command}", '', logger: Logger.new(STDOUT))
    line.run
    @output = line.output.output

    if File.exists?(state_path)
      @state = File.read(state_path)
    end

    FileUtils.rm_r(dir, force: true)
    
    return @output
  end

  def time_now
    Time.now.strftime('%m%e%y%H%M%S')
  end

end
