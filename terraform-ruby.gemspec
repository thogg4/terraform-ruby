# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'version'

Gem::Specification.new do |spec|
  spec.name          = "terraform-ruby"
  spec.version       = Version::VERSION
  spec.authors       = ["Tim Hogg"]
  spec.email         = ["thogg4@gmail.com"]

  spec.summary       = "summary"
  spec.description   = "description"
  spec.homepage      = "https://github.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ["lib"]

  spec.requirements << 'terraform'

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_runtime_dependency 'cocaine', '~> 0.5.8'
  spec.add_runtime_dependency 'posix-spawn', '~> 0.3.11'
end
