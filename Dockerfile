FROM ruby:2-alpine

RUN apk update && apk add git alpine-sdk

ENV TERRAFORM_VERSION=0.7.0
ENV TERRAFORM_SHA256SUM=a196c63b967967343f3ae9bb18ce324a18b27690e2d105e1f38c5a2d7c02038d

ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip ./
ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS ./

RUN sed -i '/terraform_${TERRAFORM_VERSION}_linux_amd64.zip/!d' terraform_${TERRAFORM_VERSION}_SHA256SUMS
RUN sha256sum -cs terraform_${TERRAFORM_VERSION}_SHA256SUMS
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin
RUN rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

RUN mkdir /gem
WORKDIR /gem
COPY Gemfile /gem
COPY terraform-ruby.gemspec /gem

RUN bundle install

COPY . /gem

CMD ["irb", "-I", "lib", "-r", "./lib/terraform.rb"]
